package com.example.simon.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun connectMe(view: View) {
//        val connectedToast = Toast.makeText(this, "Connected !", Toast.LENGTH_SHORT)
//        connectedToast.show()
        val imgurLogin = Intent(this, ImgurLoginActivity::class.java)
        startActivity(imgurLogin)
    }
}
