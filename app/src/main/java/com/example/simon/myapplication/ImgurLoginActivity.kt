package com.example.simon.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import ca.mimic.oauth2library.OAuth2Client
import ca.mimic.oauth2library.OAuthError
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException


class ImgurLoginActivity : AppCompatActivity() {

    private var httpClient: OkHttpClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imgur_login)
        fetchData()
    }

    private fun fetchData() {
        httpClient = OkHttpClient.Builder().build()
    }

    var client = OkHttpClient()
    var builder: OAuth2Client.Builder = OAuth2Client.Builder(
        "309f05707af46f3",
        "a82182dfd5171d488bacb9bc8cb1558d7149b555",
        "").okHttpClient(client)

    private class Photo {
        internal var id: String? = null
        internal var title: String? = null
    }
}